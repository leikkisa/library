// Not used much in the real world anymore because memory constraints aren't as bad and we have dynamic arrays

class LinkedList {
  constructor() {
    this.head = null
    this.tail = null
  }

  add(value) {
    const node = new Node(value)
    if (this.head === null) { this.head = node }
    if (this.tail !== null) { this.tail.next = node }
    this.tail = node
  }

  removeAt(index) {
    let prev = null
    let node = this.head
    let i = 0
    while (node !== null && i++ < index) {
      prev = node
      node = node.next
    }
    if (prev === null) { this.head = node.next }
    else { prev.next = node.next }
  }
}

class Node {
  constructor(data) {
    this.data = data
    this.next = null
  }
}

const list = new LinkedList()
list.add(1)
list.add(2)
list.add(3)
list.removeAt(1)
