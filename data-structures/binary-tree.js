// Data structure where each parent has two children
// Every node has a left and a right. They can be null
// Node Types: Root, Parent, Child, Leaf (no children)
// Left and Right are pointers to another node or null

// Binary Search Tree, every element on the left is smaller than every element on the Right

// Depth first:
// In order traversal
// Pre-order traversal
// Post-order traversal

// Breadth first:
// Level traversal

class BinaryTreeNode {
  constructor(data, left = null, right = null) {
    this.data = data
    this.left = left
    this.right = right
  }
}

function inOrderTraversal(node) {
  if(node.left) {
    inOrderTraversal(node.left)
  }
  console.log(node.data)
  if(node.right) {
    inOrderTraversal(node.right)
  }
}

function preOrderTraversal(node) {
  console.log(node.data)
  if(node.left) {
    preOrderTraversal(node.left)
  }
  if(node.right) {
    preOrderTraversal(node.right)
  }
}

function postOrderTraversal(node) {
  if(node.left) {
    postOrderTraversal(node.left)
  }
  if(node.right) {
    postOrderTraversal(node.right)
  }
  console.log(node.data)
}

function levelOrderTraversal(node) {
  const queue = [node]
  while (queue.length > 0) {
    let currentNode = queue.shift()
    console.log(currentNode.data)
    if(currentNode.left) {
      queue.push(currentNode.left)
    }
    if(currentNode.right) {
      queue.push(currentNode.right)
    }
  }
}

function levelRecursive(queue) {
  if(queue.length === 0) {
    return
  } else {
    let node = queue.shift()
    console.log(node.data)
    let nodes = []
    if(node.left) {
      nodes.push(node.left)
    }
    if(node.right) {
      nodes.push(node.right)
    }
    levelRecursive(queue.concat(nodes))
  }
}

function levelWithTracking(node) {
  let level = 1
  let Q = [[node, level]]
  while(Q.length > 0) {
    let current = Q.shift()
    let node = current[0]
    let level = current[1]
    console.log(`${level}: ${node.data}`)

    if(node.left) {
      Q.push([node.left, level + 1])
    }
    if(node.right) {
      Q.push([node.right, level + 1])
    }
  }
}

function topView(node) {
  console.log(node.data)
  let currentNode = node
  while(currentNode.left) {
    currentNode = currentNode.left
    console.log(currentNode.data)
  }
  currentNode = node
  while(currentNode.right) {
    currentNode=currentNode.right
    console.log(currentNode.data)
  }
}

let myTree = new BinaryTreeNode(10, new BinaryTreeNode(7, new BinaryTreeNode(5)), new BinaryTreeNode(15, new BinaryTreeNode(13, new BinaryTreeNode(12))))

// inOrderTraversal(myTree)
// levelOrderTraversal(myTree)
// levelWithTracking(myTree)
// levelRecursive([myTree])
// topView(myTree)
