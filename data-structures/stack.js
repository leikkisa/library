// Last in, first out

// Operations:
// push(e)
// pop() => element
// length()
// peek()

export default class Stack {
  constructor(capacity) {
    this._capacity = capacity || Infinity;
    this._storage = {};
    this._length = 0;
  }

  push(value) {
    if(this._length < this._capacity) {
      this._storage[this._length++] = value;
      return this._length;
    }
    return 'Max capacity already reached. Remove element before adding a new one.';
  };

  pop() {
    if(this._length > 0) {
      result = this._storage[--this._length]
      delete this._storage[this._length]
      return result
    }
    return 'There is nothing else left in your stack!'
  }

  peek() {
    return this._storage[this._length - 1]
  };

  length() {
    return this._length
  };
}
