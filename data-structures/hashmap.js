// Hashmap

// Like a JavaScript Object
// Key-value pairs
// aka: Map, Dictionary, Hash Table, Associated array
// No guaranteed order

// Operations:
// Add
// remove
// search
// empty
// length
// getKeys
// update (a keys value)

// Functions
// hash

export default class HashMap(size) {
  constructor() {
    this.size = size
    this.buckets = new Array(this.size)
  }

  // Example:
  // this.buckets = [undefined, undefined, [key, value, key, value ...]]

  function hash(str) {
    let sum = 0
    str.split('').forEach((char) => {
      sum += char.charCodeAt()
    })
    return sum % this.size
  }

  // called when you type in a variable in the node repl
  inspect() {
    return this.toString()
  }

  insert(value) {
    const index = hash(value)
    if (this.buckets[index]) {
      this.buckets[index].push(value)
    } else {
      this.buckets[index] = [value]
    }
  }

  search(value) {
    const index = hash(value)
    let bucket = this.buckets[index]
    const location = bucket.findIndex((element) => {
      return element === value
    })
    if (location === -1) {
      return undefined
    } else {
      let i = 0
      while (bucket[i] !== value) {
        i += 1
      }
      return bucket[i]
    }
  }
}
