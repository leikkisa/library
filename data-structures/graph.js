// Each node can have n neighbors.
// The link can be directional or bidirectional, it can be weighted
// There can be cycles, where connections form a loop. Keep track of visited nodes to prevent this.
// Use a queue for breadth-first search, and a stack for depth-first-search
// Root is the node you start with

// pseudocode
// 1. Start with root node
// 2. Maintain a list of visited Nodes V
// 3. Add current Node's neighbors to a queue
// 4. Add current node to V
// 5. Shift node from stack S
// 6. Add node to V
// 7. Add neighbors to stack S if they are not in V
// 8. If stack is not empty, go to step 5
// 9. Print visited nodes V

class GraphNode {
  constructor(id, value=null, neighborsArr=[]) {
    this.id = id
    this.value = value
    this.neighbors = neighborsArr
  }
}

class Edge {
  constructor(startNode, endNode, weight) {
    this.startNode = startNode
    this.endNode = endNode
    this.weight = weight
  }
}

class Graph {
  constructor(nodes, edges) {
    this.nodes = nodes
    this.edges = edges
  }
}

let A = new GraphNode('A')
let B = new GraphNode('B')
let C = new GraphNode('C')
let D = new GraphNode('D')
let E = new GraphNode('E')
let F = new GraphNode('F')
let G = new GraphNode('G')

const myGraph = {
  "A": ["B", "C"],
  "B": ["A", "E", "G"],
  "C": ["A", "D", "E"],
  "D": ["C"],
  "E": ["B", "C"],
  "G": ["B"],
  "Z": []
}

function bfs(node) {
  const Q = [ node ]
  const visited = [ ]
  while (Q.length > 0) {
    currentNode = Q.shift()
    visited.push(currentNode.id)
    currentNode.neighbors.forEach((neighbor) => {
      if(!visited.includes(neighbor.id)) {
        Q.push(neighbor)
      }
    })
  }
  return visited
}

bfs(myGraph["A"])

function bfsRecursive(graph, visitedNodes, queue) {
  if(queue.length === 0) {
    return visitedNodes
  } else {
    let currentNode = queue.shift()
    let neighbors = graph[currentNode]
    if(!visitedNodes.includes(currentNode)) {
      visitedNodes.push(currentNode)
    }
    neighbors.forEach(neighbor => {
      if(!visitedNodes.includes(neighbor)) {
        queue.push(neighbor)
      }
    })
    return bfsRecursive(graph, visitedNodes, queue)
  }
}
bfsRecursive(Graph, ['A'], Graph['A'])
// When transposing a while loop to a recursive function, look for which variables appear "global" to the while loop, and then those need to be passed in as variables
// Also make sure to include the exit condition
// Return recursive function call
// Iterative is faster in JavaScript due to function call
// Is faster if function call is the last thing, aka Tail Call Optimization in ES6

function dfs(node) {
  const stack = [ node ]
  const visited = [ ]
  while (Q.length > 0) {
    currentNode = Q.pop()
    visited.push(currentNode.id)
    currentNode.neighbors.forEach((neighbor) => {
      if(!visited.includes(neighbor.id)) {
        Q.push(neighbor)
      }
    })
  }
  return visited
}
