# README #

Hello! This repo is an organized collection of small chunks of code or other resources (like links, folder structures) that I might want to refer to later.

It is for my personal use and will always be a work in progress, but of course feel free to use anything if you find it useful :-)

### Algorithms ###
Algorithm solutions

### Data Structures ###
Code for setting up and using data structures

### Design Components ###
Code for setting up front-end design components (such as a menu, pagination, etc). which may include both client-side and server-side code

### Functions ###
Functions that I find useful that I might want to re-use

### Notes ###
Notes, such as on setting up my computer, commands, knowledge frameworks, etc.

### Projects ###
Code for starting a new project with a particular framework and set up

### Links ###
Links to references, tools, articles, etc. that I find useful

### SQL ###
SQL for table schemas, seeding a database, setting up a REST API, and querying data
