# Looking at log files in Bash #

Cut is similar to the split command in JavaScript

Cut by a quote and print the second part
cat access_log_20151221-101535.log | cut -d ' ' -f6 | cut -d '"' -f2

Get the unique values with a count
cat access_log_20151221-101535.log | cut -d ' ' -f6 | cut -d '"' -f2 | sort | uniq -c

DELETE
GET
POST
PUT

Equivalent
cat access_log_20151221-101535.log | awk -F ' ' '{print $6}' | sort | uniq -c

Get count of how often each URL was visited, in order of most visited:
cat access_log_20151221-101535.log | cut -d ' ' -f7 | cut -d '"' -f2 | sort | uniq -c | sort -r
