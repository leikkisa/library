# CHEATSHEET

---

# **UNIX**

`ctrl - C` to escape  
`man`  
`clear` = `ctrl-L`  
`^A` beginning of line  
`^E` end of line  
`^U` clear to beginning  
`option-click` move cursor with mouse click  

`> <file>`  save output to filename  
`>> <file>`  append output to filename  
`cat <file>`  print file contents to screen  
`diff <file1> <file2>`  difference  
`touch <file>`  new empty file  
`mv <source> <new>`  rename  
`cp <source> <new>`  copy file  
`cp -r <source dir> <new>`  copy dir recursively, includes files inside  
`rm -f <file>`  force-remove file  
`rm -rf <dir>`  remove directory and its contents  
`mkdir -p ~/foo/bar ` -p used to make 2 nested directories simultaneously  
`grep <something>`  search for characters  
`|`  pipe result to a new command ( history | grep npm )  
`&&`  combine commands, will run if the previous one succeeds  

---
# **BASH**  
`#!/bin/bash`  shebang, start all .sh files with this line to run script

## **Variables**

`$0`  name of Bash script  
`$1-$9`  first 9 arguments to the script  
`$#`  how many arguments were passed  
`$@`  all the arguments supplied  
`$RANDOM`  random number between 0 - 32767  
`var=$( command )`  set command to variable for command substitution  
`“Something something $var”`  double quotes for command substitution

## **Input**

3 methods:
* command line argument
* read input during script execution
* accept data redirected into script via STDIN  

`read varname`  assign user input into variable  
`read -p  ‘Some text: ‘ sometext`  specify a prompt on same line  
`read -sp ‘Password: ‘ pass`  makes input silent as user types

## **Arithmetic**

let a built in function for simple arithmetic
* `let a=5+4`  without quotes, no spaces
* `let “a = 5 + 4”`  with quotes, you can add spaces

expr  prints result instead of saving to variable  
* `expr 5 + 4`  must have spaces to do math
* `a=$( expr 5 + 4 )`  set result to variable

$(( double parentheses ))  set result of basic math to variable
* `a=$(( 5 + 4 ))`  spaces for readability
* `a=$((b*4))` spaces optional, variables don’t need $, no escape sign \ needed for `*`  

`${#variable}`  returns character length of variable

## **If Statements**

    if [ <some test> ]  
    then  
     <commands>  
    fi  

`[ <some test> ]`  test will return 0 for true, 1 for false  
* `=`  compares strings
* `-eq`  numerically equal
* `-ne`  numerically not equal
* `-gt`  numerically greater than
* `-lt`  numerically less than
* `-d`  file exists and is a directory
* `-e`  file exists
* `-r`  file exists and read permission granted
* `-w`  file exists and write permission granted
* `-x`  file exists and is executable
* `test`  use on command line to experiment/troubleshoot

**Elif** series of conditions with different commands  

    if [ <some test> ]
    then
      <commands>
    elif [ <some test> ]
    then
      <different commands>
    else
      <other commands>
    fi

**Boolean Operations**  only do something if multiple conditions true  
* `&&` and
* `||`  or


    if [ -r $1 ] && [ -s $1 ]
    then
      echo This is awesome.
    else
      echo Meh.
    fi

**Case Statements**

    case <variable> in
    <pattern 1>)
      <commands>
      ;;
    <pattern 2>)
      <other commands>
      ;;
    esac

## **Loops**

**While Loops**  keep executing while test is true

    while [ <some test> ]
    do
      <commands>
    done

**Until Loops** execute command until test is true

    until [ <some test> ]
    do
      <commands>
    done

**For Loops**  takes each item in a list (strings, numbers, files), runs command for each item

    for var in <list>
    do
      <commands>
    done

**Controlling Loops**  
`break`  exit the currently running loop  
`continue`  stop this iteration of a loop, begin the next iteration  
`select`  simple menu system for selecting items from a list

## **Functions**

    function_name () {
      <commands>
      echo Hello $1
    }

  —  or — 

    function function_name {
      <commands>
      echo Hello $1
    }

`function_name Argument`  
supply argument right after function name
