// Most basic search. Sequentially moves through collection looking for a matching value.

// Big O: time: O(N)

function findIndex(valuesArr, target) {
  for(let i = 0; i < valuesArr.length; i++) {
    if (valuesArr[i] == target) {
      return i
    }
  }
  return -1
}

findIndex([7, 3, 6, 1, 0], 6)
