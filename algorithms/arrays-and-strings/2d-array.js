// https://www.hackerrank.com/challenges/2d-array/problem

// Given a 6x6 array, find the hourglass shape with the greatest sum

const sampleInput = [
  [ 1, 1, 1, 0, 0, 0 ],
  [ 0, 1, 0, 0, 0, 0 ],
  [ 1, 1, 1, 0, 0, 0 ],
  [ 0, 9, 2, -4, -4, 0 ],
  [ 0, 0, 0, -2, 0, 0 ],
  [ 0, 0, -1, -2, -4, 0 ]
]

function greatestHourglass(arr2d) {
  let hourglassMax = -9 * 7 // minimum possible value
  for (let i=0; i<4; i++) {
    for (let j=0; j<4; j++) {
      let currentSum = arr2d[i][j] + arr2d[i][j + 1] + arr2d[i][j + 2] + arr2d[i+1][j+1] + arr2d[i+2][j] + arr2d[i+2][j + 1] + arr2d[i+2][j + 2]
      hourglassMax = Math.max(currentSum, hourglassMax)
    }
  }
  return hourglassMax
}
