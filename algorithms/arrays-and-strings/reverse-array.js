// Reverse an array

function reverseArr(arr) {
    const n = arr.length
    const reversed = []
    for (let i = n-1; i >= 0; i--) {
      reversed.push(arr[i])
    }
    return reversed
}
