# Steps
1. Understand the problem
  - Read it at least twice (if written)
  - Clarify inputs
    - What format is it provided in
    - Are numbers floats or integers
    - Can there be negative numbers
  - Clarify output
    - What format should it be in
    - What to return if there is no result (null? empty?)
    - Returning a number, index of number, boolean?
2. If appropriate, divide and conquer (split up into parts)
3. Come up with example (if one is not provided), inputs/outputs, start with smallest case possible
4. If appropriate, make a drawing
5. Think of edge cases
6. Solve the problem manually with 3 sets of sample data
7. Optimize manual approach
8. Pseudocode approach
9. Decide on which functions are needed
10. Create function container(s)
11. Implement code
12. Test code - run through sample inputs
13. Check edge cases
14. Refactor/Optimize
15. Determine time and space complexity
16. Would I do anything differently if the input was really huge?
  - Chunking

# Common mistakes
1. Forgetting to turn text into numbers when processing inputs

# Edge cases
1. General
  2. Empty or null - Return error? Null? Empty? As is?
  3. Input type error - return error
4. Hash map
  4. Looking up a key that doesn't exist
5. Parse strings
  5. Splitting on a character that might be found within a string
  6. Escape characters

# Types of approaches
1. For loops
2. Do while loops
  - If you might not need to loop through entire input
  - Recursive-style functions with base case
3. If statements / Switch
4. Functional programming
  - Recursion
    - Decide base case(s)
5. Dynamic programming / memoization
6. Hash maps
7. Binary tree
8. Linked List
  - Searching efficiency O(n)
9. Queue
10. Stack

# Types of problems
1. Simple recursive algorithms
2. Backtracking algorithms - depth-first recursive search (countries not next to another country of the same color on a map)
3. Divide-and-conquer algorithms - splits up the problem, uses recursion
4. Dynamic programming algorithms - one subproblem that can be repeatedly solved
5. Greedy algorithms
  - Trying to make the best possible choice at the present moment (example shortest path)
6. Branch-and-bound algorithms
7. Brute force algorithms
8. Hash map - Do I have to look up a value?
9. Search
10. Sorting
11. Finding a path

# Interviewer rubric

1. Interviewee should write relevant information about the question
2. Sample input and output
3. Ensure that the interviewee writes pseudocode
4. Use function containers / classes where appropriate
5. Check for logic
6. Thinking out loud
7. Get to a working solution, then optimize
8. Time and space complexity
9. Whiteboard management
10. Time management
11. Indentation
12. Presentation - not covering up the whiteboard
13. Was a solution reached?
14. Would the code run?

# Big O

## Factors that scale
Input	Common Factor that is Scaling
Integer	Magnitude of number
String	Length of String
Array	Length of Array
LinkedList	Number of nodes
Tree	Number of nodes
Hashtable	Number of key-value pairs
Matrix	Width and height of matrix
Graph	Number of vertices and edges
