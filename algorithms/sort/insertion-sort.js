// Compare one element only with elements before it

function insertionSort(arr) {
  const n = arr.length
  
  for (let i=1;i<n;i++) {
    if(arr[i] < arr[i-1]) {
      const currentNum = arr[i]
      let currentIndex = i-1
      while (arr[currentIndex] > currentNum) {
          arr[currentIndex + 1] = arr[currentIndex]
          currentIndex--
      }
      arr[currentIndex + 1] = currentNum
    }
  }

  return arr
}
