// https://class.outco.io/courses/technical/lectures/1950839
// Multiple pointers
// Given a bit array, return it sorted in-place (a bit array is simply an array that contains only bits, either a 1 or a 0).

function bitArraySort(arr) {
  let left = 0
  let right = arr.length - 1

  while(left < right) {
    while(arr[left] === 0) { left++ }
    while(arr[right] === 1) { right-- }
    if(left < right) {
       // swap the left and right values
      [arr[left], arr[right]] = [arr[right], arr[left]]
    }
  }

  return arr
}
