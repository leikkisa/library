// https://www.interviewcake.com/question/javascript/rectangular-love
// Calculate a rectangle that represents the overlapping area between two rectangles

function loveIntersection(r1, r2) {
  // Calculate rectangle end points
  r1.x2 = r1.x1 + r1.width
  r1.y2 = r1.y1 + r1.height
  r2.x2 = r2.x1 + r2.width
  r2.y2 = r2.y1 + r2.height

  // If the rectangles don't overlap
  if(r1.x2 <= r2.x1 || r2.x2 <= r1.x1 || r1.y2 <= r2.y1 || r2.y2 <= r1.y1) {
    return {x1: null, y1: null, width: null, height: null}
  }

  // Calculate overlap
  const xArr = [ r1.x1, r1.x2, r2.x1, r2.x2 ].sort()
  const yArr = [ r1.y1, r1.y2, r2.y1, r2.y3 ].sort()
  const result = {
    x1: xArr[1],
    width: xArr[2] - xArr[1],
    y1: yArr[1],
    height: yArr[2] - yArr[1]
  }

  return result
}
