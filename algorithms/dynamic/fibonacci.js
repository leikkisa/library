// Dynamic bottom up - most efficient in JavaScript
function fibonacci(n) {
  let memory = [0, 1, 1]
  for(i == 3; i < n; i++) {
    memory[i] = memory[i-1] + memory[i-2]
  }
  return memory
}

// Dynamic top down
var memory = [0, 1, 1]
function fibonacci_dynamic(n) {
  if (memory[n]) {
    return memory[n]
  } else {
    var result = fibonacci_dynamic(n-1) + fibonacci_dynamic(n-2)
    memory[n] = result
    return result
  }
}

// Functional
// O(2^n) - calling it twice per ns
function fibonacci_functional(n) {
  if (n==1 or n==2) {
    return 1
  }
  return fibonacci_functional(n-1) + fibonacci_functional(n-2)
}
