// Frequency counting - for problems that require tracking, counting, or looking up values quickly

// Hashtable: General all purpose use
// Array: Values in the collection that are of a small range of integer values.
// Set: If only needed to track if something exists.

// 1. Populate count with loop through collection

// twoSum
//Given an array of integers, and a target value determine if there are two integers that add to the sum.

function twoSum(numbers, target){
  let hash = {}
  let current
  for(let i = 0; i < numbers.length; i++) {
    current = numbers[i]
    if(hash[current]) { return true }
    hash[target - current] = true
  }
  return false
}
