// inputArr = [3, 6, 4, 10, 22, 8, 16, 32]
// example of longest increasing subsequences
// (3, 4, 10, 22, 32)
// (3, 6, 10, 22, 32)
// (3, 4, 8, 16, 32)
// (3, 6, 8, 16, 32)
// output - length of longest increasing subsequence = 5

// Sub-problem: Guess an answer
// Guess all answers
// Take the best one

// Top-down approach

// Figuring out the "recurrence" - how is the problem repetitive?

function longestIncreasingSubsequence(inputArr) {

}



// Not working
const input = [3, 6, 4, 10, 22, 8, 16, 32, 2, 45, 43, 44]
function longestIncreasingSubsequence2(inputArr) {
  let longest = 0
  inputArr.forEach((el, index) => {
    let current = 1
    let max = el
    for(i=index+1; i<inputArr.length; i++) {
      if (inputArr[i] > max) {
        max = inputArr[i]
        current++
      }
    }
    if (current > longest) {
      longest = current
    }
  })
  return longest
}

console.log(longestIncreasingSubsequence(input))
