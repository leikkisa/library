# Humor #

(Coding-related, of course)

[50 Nerds of Grey](https://twitter.com/50NerdsofGrey "50 Nerds of Grey")

[Bruce Lee on Programming](https://www.youtube.com/watch?v=3Ii_ALXDG70 "Bruce Lee on Programming")

[Commit Logs from Last Night](http://www.commitlogsfromlastnight.com "Commit Logs from Last Night")

[Hackernoon Coding Jokes](https://hackernoon.com/30-jokes-only-programmers-will-get-a901e1cea549)

[Hongkiat Coding Jokes](https://www.hongkiat.com/blog/programming-jokes/)

[HTTP Cats](https://http.cat/ "HTTP Status Cats")

[HTTP Dogs](https://httpstatusdogs.com/ "HTTP Status Dogs")

[Vanilla JS](http://vanilla-js.com/ "Vanilla JS")

[Wat talk by Gary Bernhardt](https://www.destroyallsoftware.com/talks/wat "Gary Bernhardt's Wat talk")

[XKCD](https://xkcd.com/ "The best thing ever")
