# Styling #

## Responsive design
- [Sizzy - see your page on multiple devices](https://sizzy.co "Sizzy")
- [StatCounter to get common screen sizes](https://statcounter.com "StatCounter")

## CSS
- [CSS Clip Path Maker](http://bennettfeely.com/clippy/ "CSS Clip Path Maker")

### Animations
- [Cubic Bezier](http://cubic-bezier.com/ "cubic-bezier")
- [Transition easings](http://easings.net/ "Easings")
