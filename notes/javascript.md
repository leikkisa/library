# JavaScript notes

## Oddities
- NaN (Not a number) is of type Number
- ``` [] + [] ``` Evaluates to ``` "" ```
- ``` {} + {} ``` Evaluates to NaN
- ``` [] + {} ``` Evaluates to an object
- ``` {} + [] ``` Evaluates to 0
- Adding a number to a string converts the number to a string and concats the two strings together

[Gary Bernhardt's Wat talk](https://www.destroyallsoftware.com/talks/wat "Gary Bernhardt's Wat talk")
