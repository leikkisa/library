# Things to look into

## Computation
- Turing Machine
- Lambda Calculus
- The Halting Problem
- Chomsky Hierarchy
  - Regular expressions
  - Python
  - C++, JavaScript
  - Turing-equivalent
  - Finite state machines
