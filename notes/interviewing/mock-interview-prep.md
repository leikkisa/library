# Mock Interview Prep
### Write down the following:
- Top 10 questions expect from recruiter/interviewers
- Questions you struggle with most
- Biggest concerns going into this one
- Debrief of prior interviews: what learned, what to highlight this round
- Job description and application materials eg resume, cover letter, cover email as well as who you'll be meeting with for this interview

### Key interview tips
- Focus first on reviewing the job description, considering all the possible ways they can ask you about those skills, preparing bullet points to those questions, and know the top reasons why you're the best person for the job, why you want the job, & why the company. These are reiterated below in "Top tips".
- Do be prepared for the initial question: Tell me about yourself and/or walk me through your resume. Hear it as "Tell me about yourself, so I know why I need to hire you."
  - Use that time to tell them why you're the best person for them to hire, not give them some long-winded personal bio.
  - The key is to highlight the 3-5 things they have to know about you and keeps them wanting to talk to you, since what you say in that 1st answer can help set the agenda/what they'll ask about next. See below regarding "can do/will do/how fit" as a possible framework for helping answer that 1st question.
  - Be concise; don't be too long-winded. Best to practice 60-90 seconds, so actual interview is less than 2 min per answer because that's a long time to be talking by yourself.

### Top tips
- Know top 5 skills they're seeking & how you have them (stories/examples to illustrate your rockstar-ness)
- Know top 5 reasons you're the BEST candidate (every answer you give helps them answer "Why should I hire you?" which can be broken into 3 parts:
  1. CAN DO -- can you do the job? do you have skills/aptitude to hit ground running or w/easy training?;
  2. WILL DO -- are you excited and motivated? do you want the job? be sure to say you do in interview;
  3. HOW FIT -- will you get along w/team and do well in company culture?
- Know your headline: what's the #1 thing you want them to remember about you? what's unique and how will you distinguish yourself from other candidates to help them know why you're the best?
- ESP: be Enthusiastic, be your best Self, be Prepared
