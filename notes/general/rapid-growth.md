# Rapid Growth #

From DropBox presentation
https://www.youtube.com/watch?v=PE4gwstWhmc

## Technical Challenges
1. Write Volume
2. ACIDity requirements
  - Atomicity, consistency, isolation, durability

## Examples
1. Started with Small scale: Single server, store files on local disk
  - Proof of concept
  - Things that broke:
    - Servers ran out of disk space
    - Servers became overloaded
  - Solution
    - Moved data to Amazon S3
    - Moved MySQL instance to a different box with separate hardware
    - Server pointed to DB and S3
  - Next things that broke
    - Capacity on the server ran out
      - Wanted to separate downloading and uploading functionality from the website
    - Clients only hitting the servers, so when there were new changes the clients would have to hit the server again to poll
      - Added notification servers (not servers), client would start pushing notifications down
    - The web server was split into two webservers, one with managed hosting doing metadata calls and one with AWS (hosting file content and accepting uploads). Metaserver and block server.
  - Next things that broke
    - Needed to add more metaservers and block servers (up from one), put a load balancer in front of the meta server
    - Block servers were doing database queries directly, repeated round trip calls over a large distance
      - Have the block servers do RPCs to the meta servers, which capsulated logic for database
    - Still only have one database
      - Add REM Cache
  - Next, keep the "shape" of the architecture and add stacks to the database, memcache, load balancers, etc. Had to modify the memcache library used for the sake of consistency.
  - Python was difficult for balancing memcache. Can only run one Python thread at a time. This means for each webserver we want one request at a time. A second request reduces speed to 60%.
    - Now every load balancer has a pair, a hot backup, and if the primary dies it uses the hot backup. Creates high availability load balancing cluster which is easier than writing custom software.
  - How to build a distributed database and how to shard things well?
    - One one server assume you can join tables, foreign constraint stuff, assume a single transaction is a single transaction
    - Users and shared folders are not sharded, trying to figure out what to do with it
    - Files are deduplicated using the hashes of each chunk
  - Added a two level hierarchy to the notservers
2.
