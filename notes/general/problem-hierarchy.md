# Problem hierarchies #

These are like Maslow's hierarchy, but for a company's problems

The most basic are at the bottom.

## Examples
Planning
Performance
Correctness
Availability
Monitoring

Product Development
Capacity Planning
Testing & Release Procedures
Post-mortem / Root Cause Analysis
Incident response
Monitoring

## Questions to ask oneself
What problem do I wish I was working on? How do I get there?
Are the problems I'm solving this week higher quality than the problems I was solving last week?
