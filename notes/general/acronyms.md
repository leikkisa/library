# Acronyms #

- ACID: Atomicity, Consistency, Isolation, Durability
- CRUD: Create, Read, Update, Delete
- DRY: Don't Repeat Yourself
- MVC: Model, View, Controller
