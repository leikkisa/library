# Meditation

- Affirmations
  - Think of qualities to reinforce and say "I am...". For example:
    - Calm
    - Systematic
    - Present
- Body scanning
  - From top to bottom, notice the parts of the body, feel the sensations there, notice if there is any tension and relax
- Breath
  - Notice the breath
  - If thoughts wander, notice them, return to the breath
  - Breathe deeply, and into the lower lungs
