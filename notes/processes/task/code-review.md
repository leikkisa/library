# Code Review #

1. Look at git diff
2. Look at README
3. If available, review live site
4. Look at package.json
5. Start at entry point, then review app
6. Review folder structure, file and variable naming
7. Code style
8. Check error handling
9. Check tests
