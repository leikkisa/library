# Thought traps #

- Jumping to conclusions / forecasting / mind reading
- Worst case scenario
- It's all my fault
- Harsh critic
- Black & white thinking
- Confusing thoughts with actual probability
- Confusing thoughts with actions
- If it "feels" true, it must be true
