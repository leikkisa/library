CREATE TABLE users (
  id serial PRIMARY KEY,
  username varchar(255) UNIQUE NOT NULL,
  email varchar(255) UNIQUE NOT NULL,
  password varchar(255) NOT NULL,
  name varchar(255),
  photo_url varchar(255) default 'img/rainbow_smiley_face.jpg',
  join_date timestamp default current_timestamp
);
