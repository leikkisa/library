CREATE TABLE comment (
  id serial PRIMARY KEY,
  user_id INTEGER REFERENCES users (id) NOT NULL,
  post_id INTEGER REFERENCES post (id) NOT NULL,
  title varchar(201) NOT NULL,
  content text NOT NULL,
  published_date timestamp default current_timestamp
);
