// Example of Object Oriented Programming in ES5

var Yurt = function(ribs) {
  this.ribs = ribs;
}
Yurt.prototype.countRibs = function(){
  if(this.ribs > 10) {
    console.log("Yum!");
  }
}
var myHouse= new Yurt(50);
myHouse.countRibs() //"Yum!"
